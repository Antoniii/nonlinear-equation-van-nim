# nim c -r hords.nim

import math

proc f(x: float64): float64 =
    return pow(x,2)*cos(2*x)+1

proc hord(a: var float64, b: var float64): float64 =
    let eps = 1e-3
    var x, c: float64
    while abs(b-a) > eps: 
        c = a - f(a)*(b-a)/(f(b)-f(a))
        echo c
        if f(a)*f(c) > 0:
            a = c
        else:
            b = c     
    x = (a+b)/2
    return x 

var 
    a = -3.0
    b = -4.0

echo "root is: ", hord(a,b) # -3.958914490988169


# Numerical solutions

# x ≈ ± 5.48114120661372...
# x ≈ ± 3.95891449098817...
# x ≈ ± 2.25743797127130...
# x ≈ ± 1.18320719655542...
